<?php

const
CLASSES_PATH = './Classes',
CONFIGURATIONS_PATH = './Configurations';

// Loading DOM class
require_once(CLASSES_PATH . '/DOM.php');

// Autoloading another classes
spl_autoload_register(function ($class) {
    require(CLASSES_PATH . '/' . $class . '.php');
});

// New XML reader instance
$reader = new XMLSimpleReader();

//var_dump($reader->getXMLFiles());

$queries = $reader->readXMLData(CONFIGURATIONS_PATH . "/klauco.xml");

foreach ($queries as $query) {

    // If query is disabled, skip it
    if (isset($query)) {
        if ($query->disabled == 1) {
            continue;
        }
    }

    // Set URL
    $url = new Url($query);

    // Create ads
    $ads = new Ads($url);

    // Set pages limit
    if (isset($query->pagesLimit)) {
        $ads->setPagesLimit($query->pagesLimit);
    }

    // Set title
    if (isset($query->title)) {
        $ads->setTitle($query->title);
    }

    // Set items limit
    if (isset($query->itemsLimit)) {
        $ads->setItemsLimit($query->itemsLimit);
    }

    // Render ads
    echo $ads->render();

}
