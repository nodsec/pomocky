<?php
/**
 * @package    Bazos.MainObjectClass
 *
 * @author     Mgr. Rene Klauco, PhD.
 * @copyright  Copyright (C) 2008 - 2016 - All rights reserved.
 * @license    Commercial license; please see LICENSE.txt
 */

/**
 * Class MainObject
 */
class MainObject
{
    /**
     * getReflection method.
     * @return ReflectionClass
     */
    public function getReflection()
    {
        return new ReflectionClass($this);
    }
}