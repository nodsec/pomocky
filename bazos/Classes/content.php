<?php

/**
 * @package    Joomla.Templates.IS
 *
 * @author     Mgr. Rene Klauco, PhD.
 * @copyright  Copyright (C) 2008 - 2016 - All rights reserved.
 * @license    Commercial license; please see LICENSE.txt
 */
class Content
{

    private $url;
    private $filter;



    /**
     * setUrl method.
     *
     * @param $url
     */
    public function setUrl($url)
    {
        $this->url = $url;
    }



    /**
     * getUrl method.
     *
     * @return mixed
     */
    public function getUrl()
    {
        return $this->url;
    }



    /**
     * getUrlContent method.
     *
     * @return bool|mixed
     */
    public function getUrlContent()
    {
        if (!$this->url) {
            throw new RuntimeException('Missing URL required for download the content.');
        }

        // curl init
        $ch = curl_init();

        //curl configure
        curl_setopt($ch, CURLOPT_URL, $this->url);
        curl_setopt($ch, CURLOPT_USERAGENT, 'Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1; .NET CLR 1.1.4322)');
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 5);
        curl_setopt($ch, CURLOPT_TIMEOUT, 5);

        // get data
        $data = curl_exec($ch);
        $httpcode = curl_getinfo($ch, CURLINFO_HTTP_CODE);

        // close curl
        curl_close($ch);

        return ($httpcode >= 200 && $httpcode < 300) ? $data : false;
    }

}
