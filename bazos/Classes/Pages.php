<?php

/**
 * @package    Bazos.PagesClass
 *
 * @author     Mgr. Rene Klauco, PhD.
 * @copyright  Copyright (C) 2008 - 2016 - All rights reserved.
 * @license    Commercial license; please see LICENSE.txt
 */
class Pages
{
    private $url;
    private $pages = array();



    /**
     * Pages constructor.
     *
     * @param Url $url
     */
    public function __construct(Url $url)
    {
        $this->url = $url->getFullURL();

        $html = file_get_html($this->url);
        $listanizerat = ($html->find('table[class="listainzerat"] tbody tr td'));
        $listanizerattext = $listanizerat[0]->nodes[3]->_[4];

        $totalPages = isset(explode(' ', $listanizerattext)[5]) ? explode(' ', $listanizerattext)[5] : 0;

        $this->pages[] = 0;
        for ($i = 1; $i <= $totalPages; $i++) {
            if ($i % 15 == 0) {
                $this->pages[] = $i;
            }
        }
    }



    /**
     * getPages method.
     *
     * @return array
     */
    public function getPages()
    {
        return $this->pages;
    }
}